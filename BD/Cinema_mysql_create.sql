CREATE TABLE `movies` (
	`id` int(11) NOT NULL,
	`title` varchar(45) NOT NULL,
	`description` varchar(250) NOT NULL,
	`rent_start` DATE NOT NULL,
	`rent_end` DATE NOT NULL,
	`duration` int(11) NOT NULL,
	`genre` varchar(16) NOT NULL,
	`rating` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `session` (
	`id` int(11) NOT NULL,
	`movie_id` int(11) NOT NULL,
	`time` DATE NOT NULL,
	`price` DECIMAL NOT NULL,
	`hall_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `hall` (
	`id` int(11) NOT NULL,
	`hall_name` varchar(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `rows` (
	`id` int(11) NOT NULL,
	`row_number` int(11) NOT NULL,
	`seat_quantity` int(11) NOT NULL,
	`hall_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `tickets` (
	`id` bigint NOT NULL,
	`session_id` int(11) NOT NULL,
	`user_id` int(11) NOT NULL,
	`row_number` int(11) NOT NULL,
	`seat_number` int(11) NOT NULL,
	`is_sold` bool NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
	`id` int(11) NOT NULL,
	`login` varchar(45) NOT NULL,
	`password` varchar(45) NOT NULL,
	`first_name` varchar(45) NOT NULL,
	`last_name` varchar(45) NOT NULL,
	`email` varchar(45) NOT NULL,
	`sex` varchar(45) NOT NULL,
	`birthday` DATE NOT NULL,
	`role_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `role` (
	`id` int(11) NOT NULL,
	`role_name` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `session` ADD CONSTRAINT `session_fk0` FOREIGN KEY (`movie_id`) REFERENCES `movies`(`id`);

ALTER TABLE `session` ADD CONSTRAINT `session_fk1` FOREIGN KEY (`hall_id`) REFERENCES `hall`(`id`);

ALTER TABLE `rows` ADD CONSTRAINT `rows_fk0` FOREIGN KEY (`hall_id`) REFERENCES `hall`(`id`);

ALTER TABLE `tickets` ADD CONSTRAINT `tickets_fk0` FOREIGN KEY (`session_id`) REFERENCES `session`(`id`);

ALTER TABLE `tickets` ADD CONSTRAINT `tickets_fk1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`);

ALTER TABLE `user` ADD CONSTRAINT `user_fk0` FOREIGN KEY (`role_id`) REFERENCES `role`(`id`);

userticketticket