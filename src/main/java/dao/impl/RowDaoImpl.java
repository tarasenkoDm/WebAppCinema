package dao.impl;

import model.Hall;
import model.Row;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import static dao.SQLs.*;


public class RowDaoImpl extends CrudDAO<Row> {

    private static RowDaoImpl crudDAO;

    public RowDaoImpl() {
        super(Row.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Row row) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROW);
        preparedStatement.setInt(1, row.getRowNumber());
        preparedStatement.setInt(2, row.getSeatNumber());
        preparedStatement.setInt(3, row.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Row row) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROW, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, row.getRowNumber());
        preparedStatement.setInt(2, row.getSeatNumber());
        preparedStatement.setInt(3,row.getHall().getId());
        return preparedStatement;
    }

    @Override
    protected List<Row> readAll(ResultSet resultSet) throws SQLException {
        List<Row> result = new LinkedList<>();
        Row row = null;
        while (resultSet.next()) {
            row = new Row();
            row.setId(resultSet.getInt("id"));
            row.setRowNumber(resultSet.getInt("row_number"));
            row.setSeatNumber(resultSet.getInt("seat_quantity"));
            row.setHall(new HallDaoImpl().getById(resultSet.getInt("hall_id")));
            result.add(row);
        }
        return result;
    }
}
