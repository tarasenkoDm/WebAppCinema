package dao.impl;

import model.User;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import static dao.SQLs.INSERT_USER;
import static dao.SQLs.UPDATE_USER;

public class UserDaoImpl extends CrudDAO<User> {

    private static UserDaoImpl crudDao;

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User user) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getFirstName());
        preparedStatement.setString(4, user.getLastName());
        preparedStatement.setString(5, user.getEmail());
        preparedStatement.setString(6, user.getSex());
        preparedStatement.setDate(7, Date.valueOf(user.getBirthday()));
        preparedStatement.setInt(8, user.getRole().getId());
        preparedStatement.setInt(9, user.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User user) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);

        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getFirstName());
        preparedStatement.setString(4, user.getLastName());
        preparedStatement.setString(5, user.getEmail());
        preparedStatement.setString(6, user.getSex());
        preparedStatement.setDate(7, Date.valueOf(user.getBirthday()));
        preparedStatement.setInt(8, user.getRole().getId());

        return preparedStatement;

    }

    @Override
    protected List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> result = new LinkedList<>();
        User user = null;
        while (resultSet.next()){
            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setEmail(resultSet.getString("email"));
            user.setSex(resultSet.getString("sex"));
            user.setBirthday(resultSet.getDate("birthday").toLocalDate());
            user.setRole(new RoleDaoImpl().getById(resultSet.getInt("role_id")));
            result.add(user);
        }
        return result;
    }
}
