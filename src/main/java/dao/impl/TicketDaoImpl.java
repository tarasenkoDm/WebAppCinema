package dao.impl;

import model.Ticket;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_TICKET;
import static dao.SQLs.UPDATE_TICKET;

public class TicketDaoImpl extends CrudDAO<Ticket> {
    public TicketDaoImpl() {
        super(Ticket.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket ticket) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKET);
        preparedStatement.setInt(1, ticket.getSession().getId());
        preparedStatement.setInt(2,ticket.getUser().getId());
        preparedStatement.setInt(3, ticket.getRowNumber());
        preparedStatement.setInt(4,ticket.getSeatNumber());
        preparedStatement.setBoolean(5,ticket.isSold());
        preparedStatement.setInt(6, ticket.getId());

        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket ticket) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TICKET, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, ticket.getSession().getId());
        preparedStatement.setInt(2,ticket.getUser().getId());
        preparedStatement.setInt(3,ticket.getRowNumber());
        preparedStatement.setInt(4,ticket.getSeatNumber());
        preparedStatement.setBoolean(5,ticket.isSold());

        return preparedStatement;
    }

    @Override
    protected List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> result = new LinkedList<>();
        Ticket ticket = null;
        while (resultSet.next()) {
            ticket = new Ticket();
            ticket.setId(resultSet.getInt("id"));
            ticket.setSession(new SessionDaoImpl().getById(resultSet.getInt("session_id")));
            ticket.setUser(new UserDaoImpl().getById(resultSet.getInt("users_id")));
            ticket.setRowNumber(resultSet.getInt("row_number"));
            ticket.setSeatNumber(resultSet.getInt("seat_number"));
            ticket.setSold(resultSet.getBoolean("is_sold"));

            result.add(ticket);
        }
        return result;
    }
}
