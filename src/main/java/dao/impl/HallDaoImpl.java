package dao.impl;

import model.Hall;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import static dao.SQLs.*;

public class HallDaoImpl extends CrudDAO<Hall>{

    public HallDaoImpl() {
        super(Hall.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall hall) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HALL);
        preparedStatement.setString(1, hall.getHallName());
        preparedStatement.setInt(2, hall.getId());

        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall hall) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HALL, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, hall.getHallName());

        return preparedStatement;
    }

    @Override
    protected List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> result = new LinkedList<>();
        Hall hall = null;
        while (resultSet.next()){
            hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setHallName(resultSet.getString("hall_name"));
            result.add(hall);
        }
        return result;
    }
}
