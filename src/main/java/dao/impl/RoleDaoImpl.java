package dao.impl;

import dao.SQLs;
import model.Role;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_ROLE;

public class RoleDaoImpl extends CrudDAO<Role> {

    private static RoleDaoImpl crudDao;

    public RoleDaoImpl() {
        super(Role.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Role role) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SQLs.UPDATE_ROLE);
        preparedStatement.setString(1, role.getRoleName());
        preparedStatement.setInt(2, role.getId());

        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Role role) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROLE, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, role.getRoleName());

        return preparedStatement;
    }

    @Override
    protected List<Role> readAll(ResultSet resultSet) throws SQLException {
        List<Role> result = new LinkedList<>();
        Role role = null;
        while (resultSet.next()){
            role = new Role();
            role.setId(resultSet.getInt("id"));
            role.setRoleName(resultSet.getString("role_name"));
            result.add(role);
        }
        return result;
    }
}
