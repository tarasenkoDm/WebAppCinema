package dao.impl;

import model.Session;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.*;

public class SessionDaoImpl extends CrudDAO<Session> {
    public SessionDaoImpl() {
        super(Session.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Session session) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION);
        preparedStatement.setDate(1, Date.valueOf(session.getSessionTime()));
        preparedStatement.setTime(2, Time.valueOf(session.getTimeStart()));
        preparedStatement.setDouble(3, session.getPrice());
        preparedStatement.setInt(4, session.getHall().getId());
        preparedStatement.setInt(5, session.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Session session) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, session.getMovie().getId());
        preparedStatement.setDate(2, Date.valueOf(session.getSessionTime()));
        preparedStatement.setTime(3, Time.valueOf(session.getTimeStart()));
        preparedStatement.setDouble(4, session.getPrice());
        preparedStatement.setInt(5, session.getHall().getId());
        return preparedStatement;
    }

    @Override
    protected List<Session> readAll(ResultSet resultSet) throws SQLException {
        List<Session> result = new LinkedList<>();
        Session session = null;
        while (resultSet.next()) {
            session = new Session();
            session.setId(resultSet.getInt("id"));
            session.setMovie(new MovieDaoImpl().getById(resultSet.getInt("movie_id")));
            session.setSessionTime(resultSet.getDate("time").toLocalDate());
            session.setTimeStart(resultSet.getTime("time_start").toLocalTime());
            session.setPrice(resultSet.getDouble("price"));
            session.setHall(new HallDaoImpl().getById(resultSet.getInt("hall_id")));
            result.add(session);
        }
        return result;
    }

//    public List<Session> sessionListByHallId(Integer id)throws SQLException{
//
//        DataSource dataSource = DataSource.getInstance();
//        String sql = String.format(SELECT_SESSION_BY_MOVIE_ID, Session.class.getSimpleName());
//        List result = null;
//        try (Connection connection = dataSource.getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(sql);
//             ResultSet resultSet = preparedStatement.executeQuery()){
//            result = readAll(resultSet);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
}
