package validation;

import dto.UserDTO;
import service.impl.UserServiceImpl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

    private  Pattern pattern;
    private  Matcher matcher;

    private final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
                    "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    public  boolean validate(String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public boolean uniqEmail(String email){
        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();
        for (UserDTO userDTO : userDTOList) {
            if(email.equals(userDTO.getEmail())){
                return false;
            }
        }
        return  true;
    }

    public boolean uniqEmail(String email, Integer id){
        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();
        for (UserDTO userDTO : userDTOList) {
            if(email.equals(userDTO.getEmail()) && !id.equals(userDTO.getId())){
                return false;
            }
        }
        return true;
    }

}
