package validation;

import dto.HallDTO;
import service.impl.HallServiceImpl;

import java.util.List;

public class HallValidation {

    public boolean checkExistingHall(String name, Integer id) {

        List<HallDTO> hallDTOs = HallServiceImpl.getInstance().getAll();
        for (HallDTO hallDTO : hallDTOs) {
            if (name.equals(hallDTO.getHallName())&& !id.equals(hallDTO.getId())) {
                return false;
            }
        }
        return true;
    }

    public boolean checkExistingHall(String name){
        List<HallDTO> hallDTOs = HallServiceImpl.getInstance().getAll();
        for (HallDTO hallDTO : hallDTOs) {
            if (name.equals(hallDTO.getHallName())) {
                return false;
            }
        }
        return true;
    }
}
