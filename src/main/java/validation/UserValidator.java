package validation;

import dto.UserDTO;
import service.impl.UserServiceImpl;

import java.util.List;

public class UserValidator {
    public boolean checkExistingUser(String login, Integer id) {
        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();
        for (UserDTO userDTO : userDTOList) {
            if (login.equals(userDTO.getLogin()) && !id.equals(userDTO.getId())) {
                return false;
            }
        }
        return true;
    }

    public boolean checkExistingUser(String login) {
        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();
        for (UserDTO userDTO : userDTOList) {
            if (login.equals(userDTO.getLogin())) {
                return false;
            }
        }
        return true;
    }

    public boolean checkFormComplete(UserDTO userDTO) {
        if (userDTO.getLogin().equals("") ||
                userDTO.getPassword().equals("") ||
                userDTO.getFirstName().equals("") ||
                userDTO.getLastName().equals("") ||
                userDTO.getEmail().equals("")) {
            return false;
        }
        return true;
    }

    public String checkUserForm(UserDTO userDTO) {

        boolean uniqUser;
        boolean uniqEmail;

        if (userDTO.getId() != null) {
            uniqUser = checkExistingUser(userDTO.getLogin(), userDTO.getId());
        } else {
            uniqUser = checkExistingUser(userDTO.getLogin());
        }
        if (!uniqUser) {
            return "loginErr";
        }

        if (!checkFormComplete(userDTO)) {
            return "formErr";
        }

        EmailValidator emailValidator = new EmailValidator();
        if (!emailValidator.validate(userDTO.getEmail())) {
            return "emailErr";
        }

        if(userDTO.getId()!=null){
            uniqEmail = emailValidator.uniqEmail(userDTO.getEmail(), userDTO.getId());
        }else{
            uniqEmail = emailValidator.uniqEmail(userDTO.getEmail());
        }
        if(!uniqEmail){
            return "emailExist";
        }

        return "true";
    }
}
