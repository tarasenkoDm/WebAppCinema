package validation;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import java.util.List;

public class MovieValidator {
    public boolean checkExistingMovie(String description, Integer id) {

        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        for (MovieDTO movieDTO : movieDTOList) {
            if (description.equals(movieDTO.getDescription()) && (!id.equals(movieDTO.getId()))) {
                return false;
            }
        }
        return true;
    }

    public boolean checkExistingMovie(String description) {
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        for (MovieDTO movieDTO : movieDTOList) {
            if (description.equals(movieDTO.getDescription())) {
                return false;
            }
        }
        return true;
    }
}
