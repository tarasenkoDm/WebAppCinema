package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;
import validation.MovieValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "MovieEditAdminServlet", urlPatterns = "/admin/movieEditAdm")
public class MovieEditAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        Integer id = Integer.parseInt(request.getParameter("id"));
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(id);
        MovieValidator movieValidator  = new MovieValidator();
        if(movieValidator.checkExistingMovie(request.getParameter("description"), id)){

            movieDTO.setTitle(request.getParameter("title"));
            movieDTO.setDescription(request.getParameter("description"));
            movieDTO.setDuration(Integer.parseInt(request.getParameter("duration")));
            String start = request.getParameter("rentStart");
            Date rentStart = Date.valueOf(start);
            movieDTO.setRentStart(rentStart.toLocalDate());
            String end = request.getParameter("rentEnd");
            Date rentEnd = Date.valueOf(end);
            movieDTO.setRentEnd(rentEnd.toLocalDate());
            movieDTO.setGenre(request.getParameter("genre"));
            movieDTO.setRating(Integer.parseInt(request.getParameter("rating")));

            MovieServiceImpl.getInstance().update(movieDTO);

            List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();

            request.setAttribute("movieDTOList", movieDTOList);

            request.getRequestDispatcher("/pages/admin/moviesAdm.jsp").forward(request, response);

        }else{
            String description = request.getParameter("description");
            String str = " - already exist, please make the other description";

            request.setAttribute("descriptionExist", str);
            request.setAttribute("description", description);

            doGet(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Integer id = Integer.parseInt(request.getParameter("id"));
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(id);

        request.setAttribute("movieDTO", movieDTO);

        request.getRequestDispatcher("/pages/admin/movieEditAdm.jsp").forward(request, response);
    }
}
