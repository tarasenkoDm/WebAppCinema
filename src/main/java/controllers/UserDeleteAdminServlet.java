package controllers;

import dto.UserDTO;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "UserDeleteAdminServlet", urlPatterns = "/admin/userDeleteAdm")
public class UserDeleteAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Integer id = Integer.parseInt(request.getParameter("id"));
        UserServiceImpl.getInstance().delete(id);

        List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();

        request.setAttribute("userDTOList", userDTOList);
        request.getRequestDispatcher("/pages/admin/usersAdm.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
