package controllers;

import dto.HallDTO;
import dto.RowDTO;
import model.Hall;
import model.Row;
import service.impl.HallServiceImpl;
import service.impl.RowServiceImpl;
import validation.HallValidation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "HallEditAdminServlet", urlPatterns = "/admin/hallEditAdm")
public class HallEditAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        edit HallName
        request.setCharacterEncoding("UTF-8");
        Integer id = Integer.parseInt(request.getParameter("id"));
        HallDTO hallDTO = HallServiceImpl.getInstance().getById(id);

        HallValidation hallValidation = new HallValidation();
        if (hallValidation.checkExistingHall(request.getParameter("hallName"), id)) {
            hallDTO.setHallName(request.getParameter("hallName"));
            HallServiceImpl.getInstance().update(hallDTO);

            //        edit Row and Seats
            Integer row = Integer.parseInt(request.getParameter("rowNumber"));
            Integer seats = Integer.parseInt(request.getParameter("seatNumber"));
            RowDTO rowDTO = new RowDTO();
            List<RowDTO> rowDTOList = RowServiceImpl.getInstance().getAll();
            for (RowDTO rowDTOs : rowDTOList) {
                if (rowDTOs.getHall().getId().equals(id)) {
                    rowDTO = rowDTOs;
                }
            }
            rowDTO.setRowNumber(row);
            rowDTO.setSeatNumber(seats);
            RowServiceImpl.getInstance().update(rowDTO);

            List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();

            request.setAttribute("hallDTOList", hallDTOList);
            request.setAttribute("rowDTOList", rowDTOList);

            request.getRequestDispatcher("/pages/admin/hallsAdm.jsp").forward(request, response);

        }else{
            String hallName = request.getParameter("hallName");
            String str = " already exist, please change hall's name";

            request.setAttribute("hallExist", str);
            request.setAttribute("hallName", hallName);

            doGet(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        HallDTO hallDTO = HallServiceImpl.getInstance().getById(id);

        RowDTO rowDTO = new RowDTO();
        List<RowDTO> rowDTOList = RowServiceImpl.getInstance().getAll();
        for (RowDTO rowDTOs : rowDTOList) {
            if (rowDTOs.getHall().getId().equals(id)) {
                rowDTO = rowDTOs;
            }
        }

        request.setAttribute("hallDTO", hallDTO);
        request.setAttribute("rowDTO", rowDTO);

        request.getRequestDispatcher("/pages/admin/hallEditAdm.jsp").forward(request, response);
    }
}
