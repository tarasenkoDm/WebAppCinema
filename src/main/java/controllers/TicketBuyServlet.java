package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import sendMail.SendMail;
import service.impl.TicketServiceImpl;
import service.impl.UserServiceImpl;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.StringTokenizer;

import static sendMail.SendMail.mailSend;

@WebServlet(name = "TicketBuyServlet", urlPatterns = "/user/ticketBuy")
public class TicketBuyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        Integer ticketId;
        TicketDTO ticketDTO = null;
        String tempTicketIds = request.getParameter("ticketsIdList");
        System.out.println("tickets id = " + tempTicketIds);

        StringTokenizer st = new StringTokenizer(tempTicketIds, "\\[\\,\\]\\ ");
                    while (st.hasMoreTokens()) {
                        ticketId = Integer.parseInt(st.nextToken());
                        System.out.println("ticket id = " + ticketId);
                        ticketDTO = TicketServiceImpl.getInstance().getById(ticketId);
                        System.out.println("ticket in db = "+ticketDTO);
                        ticketDTO.setSold(true);
                        TicketServiceImpl.getInstance().update(ticketDTO);
                    }

        try {
            mailSend("subject","text", UserServiceImpl.getInstance().getById(ticketDTO.getUser().getId()).getEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        response.sendRedirect(request.getContextPath() + "/pages/user/ticketBuyComplete.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
