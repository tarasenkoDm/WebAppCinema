package controllers;

import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SessionDeleteAdminServlet", urlPatterns = "/admin/sessionDeleteAdm")
public class SessionDeleteAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Integer id = Integer.parseInt(request.getParameter("id"));
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(id);
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(sessionDTO.getMovie().getId());

        SessionServiceImpl.getInstance().delete(id);

        List<SessionDTO> sessionDTOList = SessionServiceImpl.getInstance().getAll();
        for (int i = sessionDTOList.size() - 1; i > -1; i--) {
            if (sessionDTOList.get(i).getMovie().getId() != movieDTO.getId()) {
                sessionDTOList.remove(i);
            }
        }

        request.setAttribute("sessionDTOList", sessionDTOList);
        request.setAttribute("movieDTO", movieDTO);

        request.getRequestDispatcher("/pages/admin/sessionAdm.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
