package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SearchMovieServlet", urlPatterns = "/searchMovie")
public class SearchMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        for (MovieDTO movieDTO : movieDTOList) {

            if (request.getParameter("title").equals(movieDTO.getTitle())) {
                request.setAttribute("movieDTO", movieDTO);
            }
        }

        request.getRequestDispatcher("/pages/common/movie.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
