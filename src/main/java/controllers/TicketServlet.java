package controllers;

import dto.RowDTO;
import dto.SessionDTO;
import dto.TicketDTO;
import service.impl.RowServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TicketServlet", urlPatterns = "/ticket")
public class TicketServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int sessionId = Integer.parseInt(request.getParameter("id"));
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);
        RowDTO rowDTO = null;

        List<RowDTO> rowDTOList = RowServiceImpl.getInstance().getAll();
        for (RowDTO dto : rowDTOList) {
            if (dto.getHall().getId().equals(sessionDTO.getHall().getId())) {
                       rowDTO = dto;         
            }
        }
        List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll();
        for (int i = ticketDTOList.size()-1; i > -1 ; i--) {
            if(ticketDTOList.get(i).getSession().getId()!= sessionId || !ticketDTOList.get(i).isSold()){
                ticketDTOList.remove(i);
            }
        }
        if(ticketDTOList.size()==0){
            ticketDTOList=null;
        }

        request.setAttribute("rowDTO", rowDTO);
        request.setAttribute("sessionDTO", sessionDTO);
        request.setAttribute("ticketDTOList", ticketDTOList);
        request.getRequestDispatcher("/pages/common/ticket.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
