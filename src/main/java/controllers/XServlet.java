//package controllers;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * @author Tarasenko Dmitriy <lordtda@gmail.com>
// * @version 0.1
// */
//@WebServlet(name = "XServlet", urlPatterns = "/xxx")
//public class XServlet extends HttpServlet {
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        request.getParameter("id");
//
//        request.getRequestDispatcher("/pages/common/xxx.jsp").forward(request, response);
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//        doPost(request, response);
//    }
//}
