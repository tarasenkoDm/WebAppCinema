package controllers;

import dto.SessionDTO;
import dto.TicketDTO;
import dto.UserDTO;
import helpers.TicketSaver;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@WebServlet(name = "TicketSelectServlet", urlPatterns = "/user/ticketSelect")
public class TicketSelectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        Integer idSessionDTO = Integer.parseInt(request.getParameter("idSessionDTO"));
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(idSessionDTO);
        Enumeration parameterNames = request.getParameterNames();
        TicketSaver ts = new TicketSaver();
        while (parameterNames.hasMoreElements()) {
            String paramName = (String) parameterNames.nextElement();
            if (paramName.startsWith("ticket")) {
                String[] paramValues = request.getParameterValues(paramName);
                ts.saveTicket(userDTO,sessionDTO, paramValues);
            }
        }
        ts.idsForBookingTicket();

        request.setAttribute("ticketDTOList", ts.getTicketDTOListCurrentUser());
        request.setAttribute("ticketsIdList", ts.getTicketsIdList());
        request.setAttribute("ticketDTO", ts.getTicketDTOListCurrentUser().get(0));
        request.setAttribute("allPrice", ts.getAllPrice());
        request.getRequestDispatcher("/pages/user/ticketSelect.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);

    }
}
