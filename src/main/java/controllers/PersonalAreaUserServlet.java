package controllers;

import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PersonalAreaUserServlet", urlPatterns = "/user/personalAreaUser")
public class PersonalAreaUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll();
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        for (int i = ticketDTOList.size() - 1; i > -1; i--) {
            if ((!ticketDTOList.get(i).getUser().getId().equals(userDTO.getId())) || !ticketDTOList.get(i).isSold()) {
                ticketDTOList.remove(i);
            }
        }

        if (ticketDTOList.size() == 0) {
            ticketDTOList = null;
        }

        request.setAttribute("ticketDTOList", ticketDTOList);
        request.setAttribute("userDTO", userDTO);
        request.getRequestDispatcher("/pages/user/personalAreaUser.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
