package controllers;

import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import model.Hall;
import model.Session;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@WebServlet(name = "SessionEditAdminServlet", urlPatterns = "/admin/sessionEditAdm")
public class SessionEditAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        Integer id = null;
        Integer sessionId = Integer.parseInt(request.getParameter("sessionId"));

        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);

        String sessionT = request.getParameter("sessionTime");
        Date sessionTime = Date.valueOf(sessionT);
        sessionDTO.setSessionTime(sessionTime.toLocalDate());
        String sessionSWithoutSecond = request.getParameter("timeStart");
        String sessionS = sessionSWithoutSecond + ":00";
        Time sessionStart = Time.valueOf(sessionS);
        sessionDTO.setTimeStart(sessionStart.toLocalTime());
        sessionDTO.setPrice(Double.parseDouble(request.getParameter("price")));

        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        for (HallDTO hallDTO : hallDTOList) {
            if (request.getParameter("hallName").equals(hallDTO.getHallName())) {
                sessionDTO.setHall(hallDTO);
            }
        }

        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        for (MovieDTO movieDTO : movieDTOList) {
            if (request.getParameter("title").equals(movieDTO.getTitle())) {
                sessionDTO.setMovie(movieDTO);
                id = movieDTO.getId();
            }
        }

        SessionServiceImpl.getInstance().update(sessionDTO);


        List<SessionDTO> sessionDTOList = SessionServiceImpl.getInstance().getAll();
        for (int i = sessionDTOList.size() - 1; i > -1; i--) {
            if (sessionDTOList.get(i).getMovie().getId() != id) {
                sessionDTOList.remove(i);
            }
        }

        request.setAttribute("sessionDTOList", sessionDTOList);

        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(id);
        request.setAttribute("movieDTO", movieDTO);

        request.getRequestDispatcher("/pages/admin/sessionAdm.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Integer sessionId = Integer.parseInt(request.getParameter("id"));
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(sessionDTO.getMovie().getId());
        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();

        request.setAttribute("movieDTO", movieDTO);
        request.setAttribute("sessionDTO", sessionDTO);
        request.setAttribute("hallDTOList", hallDTOList);

        request.getRequestDispatcher("/pages/admin/sessionEditAdm.jsp").forward(request, response);
    }
}
