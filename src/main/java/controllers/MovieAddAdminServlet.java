package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;
import validation.MovieValidator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "MovieAddAdminServlet", urlPatterns = "/admin/movieAddAdm")
public class MovieAddAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        add new Movie
        request.setCharacterEncoding("UTF-8");

        MovieValidator movieValidator = new MovieValidator();
        if (movieValidator.checkExistingMovie(request.getParameter("description"))) {

            MovieDTO movieDTO = new MovieDTO();

            movieDTO.setTitle(request.getParameter("title"));
            movieDTO.setDescription(request.getParameter("description"));
            movieDTO.setDuration(Integer.parseInt(request.getParameter("duration")));
            String start = request.getParameter("rentStart");
            Date rentStart = Date.valueOf(start);
            movieDTO.setRentStart(rentStart.toLocalDate());
            String end = request.getParameter("rentEnd");
            Date rentEnd = Date.valueOf(end);
            movieDTO.setRentEnd(rentEnd.toLocalDate());
            movieDTO.setGenre(request.getParameter("genre"));
            movieDTO.setRating(Integer.parseInt(request.getParameter("rating")));

            MovieServiceImpl.getInstance().save(movieDTO);

            List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();

            request.setAttribute("movieDTOList", movieDTOList);

            request.getRequestDispatcher("/pages/admin/moviesAdm.jsp").forward(request, response);

        }else{

            String str = " - already exist, please change description";
            String description = request.getParameter("description");
            request.setAttribute("descriptionExist", str);
            request.setAttribute("description", description);

            request.setAttribute("title", request.getParameter("title"));
            request.setAttribute("duration", Integer.parseInt(request.getParameter("duration")));
            String start = request.getParameter("rentStart");
            Date rentStart = Date.valueOf(start);
            request.setAttribute("rentStart", rentStart.toLocalDate());
            String end = request.getParameter("rentEnd");
            Date rentEnd = Date.valueOf(end);
            request.setAttribute("rentEnd", rentEnd.toLocalDate());
            request.setAttribute("genre", request.getParameter("genre"));
            request.setAttribute("rating", Integer.parseInt(request.getParameter("rating")));

            request.getRequestDispatcher("/pages/admin/movieAddAdm.jsp").forward(request, response);
        }


    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/pages/admin/movieAddAdm.jsp").forward(request, response);

    }
}