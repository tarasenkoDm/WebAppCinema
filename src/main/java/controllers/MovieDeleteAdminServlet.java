package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "MovieDeleteAdminServlet", urlPatterns = "/admin/movieDeleteAdm")
public class MovieDeleteAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Integer id = Integer.parseInt(request.getParameter("id"));
        MovieServiceImpl.getInstance().delete(id);

        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();

        request.setAttribute("movieDTOList", movieDTOList);

        request.getRequestDispatcher("/pages/admin/moviesAdm.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
