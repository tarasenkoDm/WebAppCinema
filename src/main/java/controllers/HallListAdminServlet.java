package controllers;

import dto.HallDTO;
import dto.RowDTO;
import service.impl.HallServiceImpl;
import service.impl.RowServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "HallListAdminServlet", urlPatterns = "/admin/hallsAdm")
public class HallListAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
        List<RowDTO> rowDTOList = RowServiceImpl.getInstance().getAll();

        request.setAttribute("hallDTOList", hallDTOList);
        request.setAttribute("rowDTOList", rowDTOList);

        request.getRequestDispatcher("/pages/admin/hallsAdm.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}