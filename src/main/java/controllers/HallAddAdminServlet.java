package controllers;

import dto.HallDTO;
import dto.RowDTO;
import service.impl.HallServiceImpl;
import service.impl.RowServiceImpl;
import validation.HallValidation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "HallAddAdminServlet", urlPatterns = "/admin/hallAddAdm")
public class HallAddAdminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        add Hall Name
        request.setCharacterEncoding("UTF-8");

        HallValidation hallValidation = new HallValidation();
        if (hallValidation.checkExistingHall(request.getParameter("hallName"))) {

            HallDTO hallDTO = new HallDTO(request.getParameter("hallName"));
            HallServiceImpl.getInstance().save(hallDTO);

            List<HallDTO> hallDTOList = HallServiceImpl.getInstance().getAll();
            Integer id = null;
            for (HallDTO hall : hallDTOList) {
                if (hall.getHallName().equals(request.getParameter("hallName"))) {
                    id = hall.getId();
                }

            }

//        add Row and Seats
            Integer row = Integer.parseInt(request.getParameter("rowNumber"));
            Integer seats = Integer.parseInt(request.getParameter("seatNumber"));
            RowDTO rowDTO = new RowDTO(row, seats, HallServiceImpl.getInstance().getById(id));
            RowServiceImpl.getInstance().save(rowDTO);

            List<RowDTO> rowDTOList = RowServiceImpl.getInstance().getAll();

            request.setAttribute("hallDTOList", hallDTOList);
            request.setAttribute("rowDTOList", rowDTOList);

            request.getRequestDispatcher("/pages/admin/hallsAdm.jsp").forward(request, response);

        } else {
            String str = " already exist, please rename new hall";
            String hallName = request.getParameter("hallName");

            request.setAttribute("hallExist", str);
            request.setAttribute("hallName", hallName);
            request.setAttribute("seatNumber", request.getParameter("seatNumber"));
            request.setAttribute("rowNumber", request.getParameter("rowNumber"));

            doGet(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/pages/admin/hallAddAdm.jsp").forward(request, response);

    }
}
