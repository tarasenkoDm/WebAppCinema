package controllers;

import dto.RoleDTO;
import dto.UserDTO;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;
import validation.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "UserEditServlet", urlPatterns = "/editUser")
public class UserEditServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        boolean incorrectForm = false;
        boolean incorrectBirthday = false;
        String checkUser;
        UserDTO userDTO;
        Integer id = Integer.parseInt(request.getParameter("id"));
        if (id != null) {
            userDTO = UserServiceImpl.getInstance().getById(id);
        } else {
            userDTO = (UserDTO) request.getSession().getAttribute("user");
        }

        try {
            userDTO.setLogin(request.getParameter("login"));
            userDTO.setPassword(request.getParameter("password"));
            userDTO.setFirstName(request.getParameter("firstName"));
            userDTO.setLastName(request.getParameter("lastName"));
            userDTO.setEmail(request.getParameter("email"));
            userDTO.setSex(request.getParameter("sex"));
            String birthDay = request.getParameter("birthday");
            Date birthday = Date.valueOf(birthDay);
            userDTO.setBirthday(birthday.toLocalDate());
            List<RoleDTO> roleDTOList = RoleServiceImpl.getInstance().getAll();
            for (RoleDTO roleDTO : roleDTOList) {
                if (request.getParameter("role").equals(roleDTO.getRoleName())) {
                    userDTO.setRole(roleDTO);
                }
            }
            UserValidator userValidator = new UserValidator();
            checkUser = userValidator.checkUserForm(userDTO);

            if (!checkUser.equals("true")) {
                incorrectForm = true;
            }

        } catch (IllegalArgumentException e) {

            String login = userDTO.getLogin();
            String missingBirthday = "add your birthday, please";
            request.setAttribute("exist", missingBirthday);
            request.setAttribute("login", login);
            request.setAttribute("loginExist", login);

            incorrectForm = true;
            incorrectBirthday = true;
            checkUser = "true";
        }

        if (incorrectForm) {

            if (!checkUser.equals("loginErr")) {
                request.setAttribute("login", request.getParameter("login"));
            } else {
                String loginExist = request.getParameter("login");
                String changeLogin = " - already exist, please change login";
                request.setAttribute("loginExist", loginExist);
                request.setAttribute("exist", changeLogin);
            }

            if (checkUser.equals("formErr")) {
                String str = "fill all form, please";
                request.setAttribute("exist", str);
                String login = request.getParameter("login");
                request.setAttribute("login", login);
                request.setAttribute("loginExist", login);
            }
            if (checkUser.equals("emailErr")) {
                String str = "incorrect e-mail, please fix it";
                request.setAttribute("exist", str);
                String login = request.getParameter("login");
                request.setAttribute("login", login);
                request.setAttribute("loginExist", login);
            }
            if (checkUser.equals("emailExist")) {
                String str = "this e-mail already exist, please change it";
                request.setAttribute("exist", str);
                String login = request.getParameter("login");
                request.setAttribute("login", login);
                request.setAttribute("loginExist", login);
            }
            request.setAttribute("password", request.getParameter("password"));
            request.setAttribute("firstName", request.getParameter("firstName"));
            request.setAttribute("lastName", request.getParameter("lastName"));
            request.setAttribute("email", request.getParameter("email"));

            if (!incorrectBirthday) {
                Date birthday = Date.valueOf(userDTO.getBirthday());
                request.setAttribute("birthday", birthday.toLocalDate());
            }
            doGet(request, response);
        } else {

            UserServiceImpl.getInstance().update(userDTO);
            List<UserDTO> userDTOList = UserServiceImpl.getInstance().getAll();

            UserDTO currentUser = (UserDTO) request.getSession().getAttribute("user");

            if (currentUser == null || !currentUser.getRole().getRoleName().equals("admin")) {

                for (UserDTO dto : userDTOList) {
                    if (dto.getLogin().equals(userDTO.getLogin())) {
                        userDTO = dto;
                    }
                }
                request.getSession().setAttribute("user", userDTO);
                request.setAttribute("userDTO", userDTO);
//                request.setAttribute("userDTO", request.getSession().getAttribute("user"));
                request.getRequestDispatcher("/pages/user/personalAreaUser.jsp").forward(request, response);

            } else {
                request.setAttribute("userDTOList", userDTOList);
                request.getRequestDispatcher("/pages/admin/usersAdm.jsp").forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserDTO currentUser = (UserDTO) request.getSession().getAttribute("user");

        if (currentUser.getRole().getRoleName().equals("admin")) {
            Integer id = Integer.parseInt(request.getParameter("id"));
            UserDTO userDTO = UserServiceImpl.getInstance().getById(id);
            RoleDTO roleDTO = RoleServiceImpl.getInstance().getById(userDTO.getRole().getId());

            request.setAttribute("userDTO", userDTO);
            request.setAttribute("roleDTO", roleDTO);
            request.getRequestDispatcher("/pages/admin/userEditAdm.jsp").forward(request, response);
        } else {
            UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
            RoleDTO roleDTO = RoleServiceImpl.getInstance().getById(userDTO.getRole().getId());

            request.setAttribute("userDTO", userDTO);
            request.setAttribute("roleDTO", roleDTO);
            request.getRequestDispatcher("/pages/user/userEditUser.jsp").forward(request, response);
        }
    }
}
