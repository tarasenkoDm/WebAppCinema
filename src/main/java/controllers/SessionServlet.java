package controllers;

import dao.impl.SessionDaoImpl;
import dto.MovieDTO;
import dto.SessionDTO;
import model.Session;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SessionServlet", urlPatterns = "/session")
public class SessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        List<SessionDTO> sessionDTOList = SessionServiceImpl.getInstance().getAll();

        for (int i = sessionDTOList.size()-1; i > -1 ; i--) {
            if(sessionDTOList.get(i).getMovie().getId()!= id){
                sessionDTOList.remove(i);
            }
        }

        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(id);

        request.setAttribute("sessionDTOList", sessionDTOList);
        request.setAttribute("movieDTO", movieDTO);

        request.getRequestDispatcher("/pages/common/session.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }
}
