package helpers;

import dto.SessionDTO;
import dto.TicketDTO;
import dto.UserDTO;
import service.impl.TicketServiceImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class TicketSaver {
    private Integer row;
    private Integer seat;
    private Double allPrice;
    private int countForTickets = 0;
    private List<TicketDTO> bookingTickets = new ArrayList<>();
    private List<Integer> ticketsIdList = new ArrayList<>();
    private List<TicketDTO> ticketDTOListCurrentUser = new ArrayList<>();

    public Double getAllPrice() {
        return allPrice;
    }

    public List<TicketDTO> getTicketDTOListCurrentUser() {
        return ticketDTOListCurrentUser;
    }

    public List<Integer> getTicketsIdList() {
        return ticketsIdList;
    }

    public void saveTicket(UserDTO userDTO, SessionDTO sessionDTO, String[] paramValues) {

        for (String paramValue : paramValues) {
            StringTokenizer st = new StringTokenizer(paramValue, ",");
            while (st.hasMoreTokens()) {
                row = Integer.parseInt(st.nextToken());
                seat = Integer.parseInt(st.nextToken());
                countForTickets++;
                TicketDTO ticketDTO = new TicketDTO(sessionDTO, userDTO, row, seat, false);
                bookingTickets.add(ticketDTO);
                if (!checkBookingTicketBeforeForCurrentUser(userDTO, sessionDTO, ticketDTO)) {
                    TicketServiceImpl.getInstance().save(ticketDTO);
                }
            }
        }
    }

    private boolean checkBookingTicketBeforeForCurrentUser(UserDTO userDTO, SessionDTO sessionDTO, TicketDTO ticketDTO) {

        int checkExistingTicketInDB = 0;
        List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll();
        if(ticketDTOList.size()==0){ return false;   }

        for (TicketDTO dto : ticketDTOList) {
            if (dto.getSession().equals(sessionDTO) &&
                    dto.getUser().equals(userDTO) &&
                    dto.getRowNumber().equals(row) &&
                    dto.getSeatNumber().equals(seat) &&
                    !dto.isSold()) {

            } else {
                checkExistingTicketInDB++;
            }
        }
        if (checkExistingTicketInDB == ticketDTOList.size()) {
            return false;
        }
        return true;
    }

    public void idsForBookingTicket() {
        List<TicketDTO> ticketDTOList = TicketServiceImpl.getInstance().getAll();
        for (TicketDTO bookingTicket : bookingTickets) {
            for (TicketDTO ticketDTO : ticketDTOList) {
                if (ticketDTO.getSeatNumber().equals(bookingTicket.getSeatNumber()) &&
                        ticketDTO.getRowNumber().equals(bookingTicket.getRowNumber()) &&
                        ticketDTO.getUser().equals(bookingTicket.getUser()) &&
                        ticketDTO.getSession().equals(bookingTicket.getSession())) {
                    ticketsIdList.add(ticketDTO.getId());
                    ticketDTOListCurrentUser.add(ticketDTO);
                }
            }
        }
        allPrice = ticketDTOListCurrentUser.get(0).getSession().getPrice() * countForTickets;
        allPrice = new BigDecimal(allPrice).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}