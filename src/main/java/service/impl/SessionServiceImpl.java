package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dao.impl.SessionDaoImpl;
import dto.SessionDTO;
import mapper.BeanMapper;
import model.Session;
import service.api.Service;
import java.util.List;

public class SessionServiceImpl implements Service<Integer, SessionDTO>{

        private static SessionServiceImpl service;
        private Dao<Integer, Session> sessionDao;
        private BeanMapper beanMapper;

        private SessionServiceImpl() {
            sessionDao = DaoFactory.getInstance().getSessionDao();
            beanMapper = BeanMapper.getInstance();
        }
    public static synchronized SessionServiceImpl getInstance() {
        if (service == null) {
            service = new SessionServiceImpl();
        }
        return service;
    }

    @Override
    public List<SessionDTO> getAll() {
        List<Session> sessions = sessionDao.getAll();
        List<SessionDTO> sessionDTOs = beanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    @Override
    public SessionDTO getById(Integer id) {
        Session session = sessionDao.getById(id);
        SessionDTO sessionDTO = beanMapper.singleMapper(session, SessionDTO.class);
        return sessionDTO;
    }

    @Override
    public void save(SessionDTO sessionDTO) {
        Session session = beanMapper.singleMapper(sessionDTO, Session.class);
        sessionDao.save(session);
    }

    @Override
    public void delete(Integer id) {
        Session session = beanMapper.singleMapper(sessionDao, Session.class);
        sessionDao.delete(id);
    }

    @Override
    public void update(SessionDTO sessionDTO) {
        Session session = beanMapper.singleMapper(sessionDTO, Session.class);
        sessionDao.update(session);
    }

//    public List<SessionDTO> getSessionByHallId(Integer id){
//        List<Session> sessions = SessionDaoImpl.sessionListByHallId(id);
//
//                return sessions;
//    }
}
