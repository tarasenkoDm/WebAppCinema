package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Session extends Entity<Integer>{

    private Movie movie;
    private LocalDate sessionTime;
    private LocalTime timeStart;
    private Double price;
    private Hall hall;

    public Session(){}

    public Session(Movie movie, LocalDate sessionTime, LocalTime timeStart, Double price, Hall hall) {
        this.movie = movie;
        this.sessionTime = sessionTime;
        this.timeStart = timeStart;
        this.price = price;
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public LocalDate getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(LocalDate sessionTime) {
        this.sessionTime = sessionTime;
    }

    public LocalTime getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(LocalTime timeStart) {
        this.timeStart = timeStart;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (movie != null ? !movie.equals(session.movie) : session.movie != null) return false;
        if (sessionTime != null ? !sessionTime.equals(session.sessionTime) : session.sessionTime != null) return false;
        if (timeStart != null ? !timeStart.equals(session.timeStart) : session.timeStart != null) return false;
        if (price != null ? !price.equals(session.price) : session.price != null) return false;
        return hall != null ? hall.equals(session.hall) : session.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (movie != null ? movie.hashCode() : 0);
        result = 31 * result + (sessionTime != null ? sessionTime.hashCode() : 0);
        result = 31 * result + (timeStart != null ? timeStart.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Session{" +
                "movie=" + movie +
                ", sessionTime=" + sessionTime +
                ", timeStart=" + timeStart +
                ", price=" + price +
                ", hall=" + hall +
                '}';
    }
}
