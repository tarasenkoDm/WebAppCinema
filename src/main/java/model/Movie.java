package model;

import java.time.LocalDate;

public class Movie extends Entity<Integer> {

    private String title;
    private String description;
    private Integer duration;
    private LocalDate rentStart;
    private LocalDate rentEnd;
    private String genre;
    private Integer rating;

    public Movie() {
    }

    public Movie(String title, String description, Integer duration, LocalDate rentStart, LocalDate rentEnd, String genre, Integer rating) {
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.genre = genre;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public LocalDate getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    @Override
    public String toString() {
        return "Movie{" +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", rentStart=" + rentStart +
                ", rentEnd=" + rentEnd +
                ", genre='" + genre + '\'' +
                ", rating=" + rating +
                '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Movie movie = (Movie) o;

        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;
        if (description != null ? !description.equals(movie.description) : movie.description != null) return false;
        if (duration != null ? !duration.equals(movie.duration) : movie.duration != null) return false;
        if (rentStart != null ? !rentStart.equals(movie.rentStart) : movie.rentStart != null) return false;
        if (rentEnd != null ? !rentEnd.equals(movie.rentEnd) : movie.rentEnd != null) return false;
        if (genre != null ? !genre.equals(movie.genre) : movie.genre != null) return false;
        return rating != null ? rating.equals(movie.rating) : movie.rating == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (rentStart != null ? rentStart.hashCode() : 0);
        result = 31 * result + (rentEnd != null ? rentEnd.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        return result;
    }
}
