package model;

public class Row extends  Entity<Integer> {

    private Integer rowNumber;
    private Integer seatNumber;
    private Hall hall;

    public Row(){}

    public Row(Integer rowNumber, Integer seatNumber, Hall hall) {
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.hall = hall;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "Row{" +
                "rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", hall=" + hall +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Row row = (Row) o;

        if (rowNumber != null ? !rowNumber.equals(row.rowNumber) : row.rowNumber != null) return false;
        if (seatNumber != null ? !seatNumber.equals(row.seatNumber) : row.seatNumber != null) return false;
        return hall != null ? hall.equals(row.hall) : row.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (rowNumber != null ? rowNumber.hashCode() : 0);
        result = 31 * result + (seatNumber != null ? seatNumber.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }
}
