package model;

public class Hall extends  Entity<Integer>{

    private String hallName;

    public Hall(){}

    public Hall(String hallName) {

        this.hallName = hallName;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "hallName='" + hallName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Hall hall = (Hall) o;

        return hallName != null ? hallName.equals(hall.hallName) : hall.hallName == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (hallName != null ? hallName.hashCode() : 0);
        return result;
    }
}
