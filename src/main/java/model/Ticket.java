package model;

public class Ticket extends Entity<Integer>{

    private Session session;
    private User user;
    private Integer rowNumber;
    private Integer seatNumber;
    private boolean isSold;


    public Ticket(){}

    public Ticket(Session session, User user, Integer rowNumber, Integer seatNumber, boolean isSold) {
        this.session = session;
        this.user = user;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.isSold = isSold;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "session=" + session +
                ", user=" + user +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", isSold=" + isSold +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Ticket ticket = (Ticket) o;

        if (isSold != ticket.isSold) return false;
        if (session != null ? !session.equals(ticket.session) : ticket.session != null) return false;
        if (user != null ? !user.equals(ticket.user) : ticket.user != null) return false;
        if (rowNumber != null ? !rowNumber.equals(ticket.rowNumber) : ticket.rowNumber != null) return false;
        return seatNumber != null ? seatNumber.equals(ticket.seatNumber) : ticket.seatNumber == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (rowNumber != null ? rowNumber.hashCode() : 0);
        result = 31 * result + (seatNumber != null ? seatNumber.hashCode() : 0);
        result = 31 * result + (isSold ? 1 : 0);
        return result;
    }
}
