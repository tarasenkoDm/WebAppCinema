package dto;


import model.Entity;

public class RowDTO extends Entity<Integer> {

    private Integer rowNumber;
    private Integer seatNumber;
    private HallDTO hall;

    public RowDTO() {
    }

    public RowDTO(Integer rowNumber, Integer seatNumber, HallDTO hall) {
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.hall = hall;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RowDTO rowDTO = (RowDTO) o;

        if (rowNumber != null ? !rowNumber.equals(rowDTO.rowNumber) : rowDTO.rowNumber != null) return false;
        if (seatNumber != null ? !seatNumber.equals(rowDTO.seatNumber) : rowDTO.seatNumber != null) return false;
        return hall != null ? hall.equals(rowDTO.hall) : rowDTO.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (rowNumber != null ? rowNumber.hashCode() : 0);
        result = 31 * result + (seatNumber != null ? seatNumber.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RowDTO{" +
                "rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", hall=" + hall +
                '}';
    }
}
