package dto;

import model.Entity;

import java.time.LocalDate;
import java.time.LocalTime;

public class SessionDTO extends Entity<Integer> {

    private MovieDTO movie;
    private LocalDate sessionTime;
    private LocalTime timeStart;
    private Double price;
    private HallDTO hall;

    public SessionDTO() {
    }

    public SessionDTO(MovieDTO movie, LocalDate sessionTime, LocalTime timeStart, Double price, HallDTO hall) {
        this.movie = movie;
        this.sessionTime = sessionTime;
        this.timeStart = timeStart;
        this.price = price;
        this.hall = hall;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    public LocalDate getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(LocalDate sessionTime) {
        this.sessionTime = sessionTime;
    }

    public LocalTime getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(LocalTime timeStart) {
        this.timeStart = timeStart;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "SessionDTO{" +
                "movie=" + movie +
                ", sessionTime=" + sessionTime +
                ", timeStart=" + timeStart +
                ", price=" + price +
                ", hall=" + hall +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SessionDTO that = (SessionDTO) o;

        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        if (sessionTime != null ? !sessionTime.equals(that.sessionTime) : that.sessionTime != null) return false;
        if (timeStart != null ? !timeStart.equals(that.timeStart) : that.timeStart != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        return hall != null ? hall.equals(that.hall) : that.hall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (movie != null ? movie.hashCode() : 0);
        result = 31 * result + (sessionTime != null ? sessionTime.hashCode() : 0);
        result = 31 * result + (timeStart != null ? timeStart.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }
}