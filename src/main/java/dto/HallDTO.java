package dto;

import model.Entity;

public class HallDTO extends Entity<Integer> {

    private String hallName;

    public HallDTO(){}

    public HallDTO(String hallName) {

        this.hallName = hallName;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    @Override
    public String toString() {
        return "HallDTO{" +
                "hallName='" + hallName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        HallDTO hallDTO = (HallDTO) o;

        return hallName != null ? hallName.equals(hallDTO.hallName) : hallDTO.hallName == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (hallName != null ? hallName.hashCode() : 0);
        return result;
    }
}
