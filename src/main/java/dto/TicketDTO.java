package dto;

import model.Entity;

public class TicketDTO extends Entity<Integer> {

    private SessionDTO session;
    private UserDTO user;
    private Integer rowNumber;
    private Integer seatNumber;
    private boolean isSold;

    public TicketDTO() {}

    public TicketDTO(SessionDTO session, UserDTO user, Integer rowNumber, Integer seatNumber, boolean isSold) {
        this.session = session;
        this.user = user;
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.isSold = isSold;
    }

    public SessionDTO getSession() {
        return session;
    }

    public void setSession(SessionDTO session) {
        this.session = session;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public String toString() {
        return "TicketDTO{" +
                "session=" + session +
                ", user=" + user +
                ", rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", isSold=" + isSold +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TicketDTO ticketDTO = (TicketDTO) o;

        if (isSold != ticketDTO.isSold) return false;
        if (session != null ? !session.equals(ticketDTO.session) : ticketDTO.session != null) return false;
        if (user != null ? !user.equals(ticketDTO.user) : ticketDTO.user != null) return false;
        if (rowNumber != null ? !rowNumber.equals(ticketDTO.rowNumber) : ticketDTO.rowNumber != null) return false;
        return seatNumber != null ? seatNumber.equals(ticketDTO.seatNumber) : ticketDTO.seatNumber == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (rowNumber != null ? rowNumber.hashCode() : 0);
        result = 31 * result + (seatNumber != null ? seatNumber.hashCode() : 0);
        result = 31 * result + (isSold ? 1 : 0);
        return result;
    }
}
