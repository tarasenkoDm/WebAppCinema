<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <title>CRM Login page</title>
    <%--<jsp:include page="fragments/headTag.jsp"/>
    <link rel="stylesheet" href="<c:url value="../resources/css/signin.css" />">--%>
    <link rel="stylesheet" href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />">
    <style>
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #eee;
        }

        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin .checkbox {
            font-weight: normal;
        }
        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        label {
            font-size: 16px;
        }
        .label-error {
            color: red;
        }
        .manager {
            margin-left: 5.5%;
        }
    </style>
</head>
<body>
    <div class="container">
        <form class="form-signin" action="login" method="post">
            <h2 class="form-signin-heading">Please sign in</h2>

            <label for="input-login" class="sr-only">Login</label>
            <input type="email" id="input-login" name="input-login" class="form-control" placeholder="Email" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9._-]+\.[a-z]{2,3}$" required autofocus>

            <label for="input-password" class="sr-only">Password</label>
            <input type="password" id="input-password" name="input-password" class="form-control" placeholder="Password" pattern="[A-Za-z_0-9]{4,16}" required>

            <button class="btn btn-lg btn-primary btn-block" id="signin-button" type="submit">Sign in</button>
            <br>
            <label class="col-sm-offset-2 label-error">${errorMessage}</label>
            <br>
            <label class="col-sm-offset-1 manager">Manager: avma@mail.ru / password</label>
            <label class="col-sm-offset-1">Admin: puva@mail.ru / password</label>
        </form>
    </div>
</body>
</html>
