<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Ticket select</title>
</head>
<body>
<h1>Оплата билета(ов)</h1>
<h4>${ticketDTO.user.login}, вы выбрали фильм: ${ticketDTO.session.movie.title}</h4>

<p>Зал: ${ticketDTO.session.hall.hallName}</p>
<p>Число: ${ticketDTO.session.sessionTime}</p>
<p>Время: ${ticketDTO.session.timeStart}</p>

    <table border="1">
        <thread>
            <tr>
                <th>Row</th>
                <th>Seat</th>
                <th>Price</th>
            </tr>
        </thread>
        <tbody>
        <c:set var="counter" value="0"/>
        <c:forEach items="${ticketDTOList}" var="tickets">
            <tr>
                <td><c:out value="${tickets.rowNumber}"/></td>
                <td><c:out value="${tickets.seatNumber}"/></td>
                <td><c:out value="${tickets.session.price}"/></td>
                <c:set var="counter" value="${counter+1}"/>
                    <%--<td><p><a href="${pageContext.servletContext.contextPath}/admin/hallEditAdm?id=${halls.id}">Редактировать</a></p></td>--%>
                    <%--<td><p><a href="${pageContext.servletContext.contextPath}/admin/hallDeleteAdm?id=${halls.id}">Удалить</a></p></td>--%>
            </tr>
        </c:forEach>

        </tbody>

    </table>
    количество билетов = <c:out value="${counter}"/>
    <p>стоимость = <c:out value="${allPrice}"/></p>

<form name="ticketSelect" method="post" action="${pageContext.servletContext.contextPath}/user/ticketBuy">
    <input type="hidden" name="ticketsIdList" value="${ticketsIdList}">
    <input type="submit" value="Оплатить"/>
</form>

<p><a href="${pageContext.servletContext.contextPath}/">Главная страница</a></p>

</body>
</html>
