<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>User Edit</title>
</head>
<body>

<h3>Edit user</h3>

${loginExist} ${exist}<p/>

<form name="userAddByAdmin" method="post" action="${pageContext.servletContext.contextPath}/editUser">
    <input type="hidden" name="id" value="${userDTO.id}">
    User name:<br> <input type="text" name="login" value="${userDTO.login}"/> <br/>
    Password:<br> <input type="password" name="password" value="${userDTO.password}"/> <br/>
    First name:<br> <input type="text" name="firstName" value="${userDTO.firstName}"/><br>
    Last name:<br> <input type="text" name="lastName" value="${userDTO.lastName}"/><br>
    e-mail:<br> <input type="text" name="email" value="${userDTO.email}"/><br>
    Sex:<br> <select name="sex">
    <option value="${userDTO.sex}">${userDTO.sex}</option>
    <option value="Male">Male</option>
    <option value="Female">Female</option>
</select><br>
    Birthday:<br> <input type="date" name="birthday" value="${userDTO.birthday}"/><br>
    <input type="hidden" name="role" value="${userDTO.role.roleName}">
</select><br>
    <p/>
    <input type="submit" value="Edit user"/>
</form>

<p><a href="${pageContext.servletContext.contextPath}/user/personalAreaUser">Личный кабинет</a></p>
<p><a href="${pageContext.servletContext.contextPath}/">Главная страница</a></p>

</body>
</html>
