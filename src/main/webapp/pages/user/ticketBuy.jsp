<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Buy ticket</title>
</head>
<body>
Вы выбрали билеты :

Общая стоимость:
${idForSeat}

<p><a href="${pageContext.servletContext.contextPath}/session?id=${sessionDTO.movie.id}">Выбор сеанса
    фильма ${sessionDTO.movie.title}</a></p>
<p><a href="${pageContext.servletContext.contextPath}/">Список фильмов</a></p>

<c:forEach items="${rowSeat}" var="rowSeat12">
    ${rowSeat12}
</c:forEach>
</body>
</html>
