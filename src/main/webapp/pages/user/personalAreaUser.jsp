<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Personal area</title>
</head>
<body>
<h1>Welcome ${userDTO.login}</h1>

<table border="1">
    <thread>
        <tr>
            <th>Login</th>
            <th>Password</th>
            <th>First name</th>
            <th>Last name</th>
            <th>E-mail</th>
            <th>Sex</th>
            <th>Birthday</th>
            <th>Action</th>
        </tr>
    </thread>
    <tbody>
    <tr>
        <td>${userDTO.login}</td>
        <td>${userDTO.password}</td>
        <td>${userDTO.firstName}</td>
        <td>${userDTO.lastName}</td>
        <td>${userDTO.email}</td>
        <td>${userDTO.sex}</td>
        <td>${userDTO.birthday}</td>
        <td><p><a href="${pageContext.servletContext.contextPath}/editUser">Редактировать</a></p></td>
    </tr>
    </tbody>
</table>

<c:choose>
    <c:when test="${ticketDTOList == null}">

        <p>Вы еще не совершали покупок билетов</p>

    </c:when>
    <c:otherwise>
        Купленные билеты:

        <table border="1">
            <thread>
                <tr>
                    <th>Фильм</th>
                    <th>Число</th>
                    <th>Время сеанса</th>
                    <th>Зал</th>
                    <th>Ряд</th>
                    <th>Место</th>
                    <th>Цена</th>
                </tr>
            </thread>
            <tbody>
            <c:forEach items="${ticketDTOList}" var="tickets">
                <tr>
                    <td><c:out value="${tickets.session.movie.title}"/></td>
                    <td><c:out value="${tickets.session.sessionTime}"/></td>
                    <td><c:out value="${tickets.session.timeStart}"/></td>
                    <td><c:out value="${tickets.session.hall.hallName}"/></td>
                    <td><c:out value="${tickets.rowNumber}"/></td>
                    <td><c:out value="${tickets.seatNumber}"/></td>
                    <td><c:out value="${tickets.session.price}"/></td>
                </tr>
            </c:forEach>

            </tbody>

        </table>

    </c:otherwise>
</c:choose>

<p><a href="${pageContext.servletContext.contextPath}/">Главная страница</a></p>
</body>
</html>
