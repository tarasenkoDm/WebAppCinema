<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Ticket</title>
</head>
<body>
<h1> Выбор билетов для фильма ${sessionDTO.movie.title}</h1>

<h3>Зал ${sessionDTO.hall.hallName}</h3>

<h4>Выберите места</h4>
<form name="ticketSelect" method="post" action="${pageContext.servletContext.contextPath}/user/ticketSelect">
    <input type="hidden" name="idSessionDTO" value="${sessionDTO.id}">
    <table border="1">
        <c:forEach var="row" begin="1" end="${rowDTO.rowNumber}">
            <tr>
                <td>Ряд <c:out value="${row}"/></td>
                <c:forEach var="seat" begin="1" end="${rowDTO.seatNumber}">

                    <c:if test="${ticketDTOList!=null}">
                        <c:forEach items="${ticketDTOList}" var="tickets">
                            <c:if test="${row==tickets.rowNumber && seat==tickets.seatNumber}">
                                <td><input type="checkbox" disabled><c:out value="${seat}"/></td>
                                <c:set var="counter" value="1"/>
                            </c:if>
                        </c:forEach>
                    </c:if>
                    <c:choose>
                        <c:when test="${counter!=1}">
                            <td><input type="checkbox" name="ticket${row}${seat}"value="${row},${seat}"><label>${seat}</label></td>
                        </c:when>
                        <c:otherwise>
                            <c:set var="counter" value="0"/>
                        </c:otherwise>
                    </c:choose>
                </c:forEach><br></tr>
        </c:forEach>
    </table>
    <p/>
    <input type="submit" value="Подтвердить выбор"/>
</form>

<p><a href="${pageContext.servletContext.contextPath}/session?id=${sessionDTO.movie.id}">Выбор сеанса
    фильма ${sessionDTO.movie.title}</a></p>
<p><a href="${pageContext.servletContext.contextPath}/">Список фильмов</a></p>

</body>
</html>
