<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>${movieDTO.title}</title>
</head>
<body>
<c:choose>
    <c:when test="${movieDTO==null}">
        Данного фильма нет в прокате нашего кинотеатра
    </c:when>
    <c:otherwise>
        <h1> Фильм ${movieDTO.title}</h1>

        <table border="1">
            <thread>
                <tr>
                    <th>Movie title</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Genre</th>
                    <th>Rating</th>
                </tr>
            </thread>
            <tbody>

            <tr>
                <td><c:out value="${movieDTO.title}"/></td>
                <td><c:out value="${movieDTO.description}"/></td>
                <td><c:out value="${movieDTO.duration}"/></td>
                <td><c:out value="${movieDTO.rentStart}"/></td>
                <td><c:out value="${movieDTO.rentEnd}"/></td>
                <td><c:out value="${movieDTO.genre}"/></td>
                <td><c:out value="${movieDTO.rating}"/></td>
            </tr>
            </tbody>
        </table>

        <p><a href="${pageContext.servletContext.contextPath}/session?id=${movieDTO.id}">Сеансы</a></p>

    </c:otherwise>
</c:choose>

<p><a href="${pageContext.servletContext.contextPath}/">Список фильмов</a></p>

</body>
</html>
