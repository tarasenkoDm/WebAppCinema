<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>All Movies</title>
</head>
<body>
<c:choose>
    <c:when test="${(userDTO!=null)&&(userDTO.role.roleName=='user')}">
        Здравствуйте ${userDTO.login}
        <p><a href="${pageContext.servletContext.contextPath}/user/personalAreaUser">Личный кабинет</a></p>
        <p><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></p>

    </c:when>
    <c:when test="${(userDTO!=null)&&(userDTO.role.roleName=='admin')}">
        Здравствуйте ${userDTO.login}
        <p>Чтобы перейти на страницу администрирования допишите "admin" в строке адреса</p>
        <p><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></p>
    </c:when>
    <c:when test="${(userDTO!=null)&&(userDTO.role.roleName=='casser')}">
        Здравствуйте кассир ${userDTO.login}
        <p><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></p>
    </c:when>
    <c:otherwise>
        <form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/login">
            Username: <input type="text" name="login"/> <br/>
            Password: <input type="password" name="password"/> <br/>
            <input type="submit" value="Login"/>
        </form>
        <p><a href="${pageContext.servletContext.contextPath}/addNewUser">Регистрация</a></p>
        <%--<p><a href="${pageContext.servletContext.contextPath}/registrationNewUser">Регистрация</a></p>--%>
    </c:otherwise>
</c:choose>


<form name="searchMovie" method="post" action="${pageContext.servletContext.contextPath}/searchMovie">
    Введите название фильма:
    <br> <input id="title" type="text" name="title"/> <input type="submit" value="search movie"/> <br/>
    <p/>

</form>

<h1>All Movies</h1>

<table border="1">
    <thread>
        <tr>
            <th>Movie title</th>
            <th>Description</th>
            <th>Duration</th>
            <th>Start</th>
            <th>End</th>
            <th>Genre</th>
            <th>Rating</th>
            <%--<th colspan="6">Action</th>--%>
        </tr>
    </thread>
    <tbody>
    <c:forEach items="${movieDTOList}" var="movies">
        <tr>
            <td><a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.title}</a></td>
            <td><c:out value="${movies.description}"/></td>
            <td><c:out value="${movies.duration}"/></td>
            <td><c:out value="${movies.rentStart}"/></td>
            <td><c:out value="${movies.rentEnd}"/></td>
            <td><c:out value="${movies.genre}"/></td>
            <td><c:out value="${movies.rating}"/></td>
        </tr>
    </c:forEach>

    </tbody>


</table>

</body>
</html>
