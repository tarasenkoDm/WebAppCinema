<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Session</title>
</head>
<body>
<h1> Выбор сеанса фильма ${movieDTO.title}</h1>


<table border="1">
    <thread>
        <tr>
            <th>Date</th>
            <th>Start time</th>
            <th>Price</th>
            <th>Hall</th>

        </tr>
    </thread>
    <tbody>
    <c:forEach items="${sessionDTOList}" var="sessions">
        <tr>
            <td><c:out value="${sessions.sessionTime}"/></td>
            <td><a href="${pageContext.servletContext.contextPath}/ticket?id=${sessions.id}">${sessions.timeStart}</a></td>
            <td><c:out value="${sessions.price}"/></td>
            <td><c:out value="${sessions.hall.hallName}"/></td>
        </tr>
    </c:forEach>

    </tbody>


</table>

<p><a href="${pageContext.servletContext.contextPath}/">Список фильмов</a></p>

</body>
</html>