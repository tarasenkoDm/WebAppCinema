<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>

<h3>Registration</h3>

${loginExist} ${exist}<p/>

<%--<form name="registrationNewUser" method="post" action="${pageContext.servletContext.contextPath}/registrationNewUser">--%>
<form name="addNewUser" method="post" action="${pageContext.servletContext.contextPath}/addNewUser">
    User name:<br> <input type="text" name="login" value="${login}"/> <br/>
    Password:<br> <input type="password" name="password" value="${password}"/> <br/>
    First name:<br> <input type="text" name="firstName" value="${firstName}"/><br>
    Last name:<br> <input type="text" name="lastName" value="${lastName}"/><br>
    e-mail:<br> <input type="text" name="email" value="${email}"/><br>
    Sex:<br> <select name="sex" >
    <option value="Male">Male</option>
    <option value="Female">Female</option>
</select><br>
    Birthday:<br> <input type="date" name="birthday" value="${birthday}"/><br>
    <input type="hidden" name="role" value="user">
    <p/>
    <input type="submit" value="Add new user"/>
</form>
<p><a href="${pageContext.servletContext.contextPath}/">На главную</a></p>
</body>
</html>
