<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <title>Edit hall by admin</title>
</head>
<body>

<h3>Edit hall</h3>
${hallName} ${hallExist}<p/>

<form name="editHallByAdmin" method="post" action="${pageContext.servletContext.contextPath}/admin/hallEditAdm">
    <input type="hidden" name="id" value="${hallDTO.id}">
    Hall name:<br> <input id="hallName" type="text" name="hallName" value="${hallDTO.hallName}"/> <br/>
    Rows count:<br> <input type="text" name="rowNumber" value="${rowDTO.rowNumber}"/> <br/>
    Seats in the row count:<br> <input type="text" name="seatNumber" value="${rowDTO.seatNumber}"/> <br/>
    <p/>
    <input type="submit" value="Edit hall"/>
</form>


<p><a href="${pageContext.servletContext.contextPath}/admin/hallsAdm">Список залов</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>
</body>
</html>