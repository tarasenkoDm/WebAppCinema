<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>All Users</title>
</head>
<body>
<h1>List Users</h1>

<p><a href="${pageContext.servletContext.contextPath}/addNewUser">Добавить нового пользователя</a></p>

<table border="1">
    <thread>
        <tr>
            <th>Login</th>
            <th>Password</th>
            <th>First name</th>
            <th>Last name</th>
            <th>E-mail</th>
            <th>Sex</th>
            <th>Birthday</th>
            <th>Role</th>
            <th>Action</th>
            <th>Action</th>
        </tr>
    </thread>
    <tbody>
    <c:forEach items="${userDTOList}" var="users">
        <tr>
            <td><c:out value="${users.login}"/></td>
            <td><c:out value="${users.password}"/></td>
            <td><c:out value="${users.firstName}"/></td>
            <td><c:out value="${users.lastName}"/></td>
            <td><c:out value="${users.email}"/></td>
            <td><c:out value="${users.sex}"/></td>
            <td><c:out value="${users.birthday}"/></td>
            <td><c:out value="${users.role.roleName}"/></td>
            <td><p><a href="${pageContext.servletContext.contextPath}/editUser?id=${users.id}">Редактировать</a></p></td>
            <td><p><a href="${pageContext.servletContext.contextPath}/admin/userDeleteAdm?id=${users.id}">Удалить</a></p></td>
        </tr>
    </c:forEach>

    </tbody>


</table>

<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>

</body>
</html>
