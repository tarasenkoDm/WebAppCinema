<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <title>Add new hall by admin</title>
</head>
<body>

<h3>Add new hall</h3>
${hallName} ${hallExist}<p/>

<form name="hallAddByAdmin" method="post" action="${pageContext.servletContext.contextPath}/admin/hallAddAdm">
    Hall name:<br> <input id="hallName" type="text" name="hallName"/> <br/>
    Rows count:<br> <input type="text" name="rowNumber" value="${rowNumber}"/> <br/>
    Seats in the row count:<br> <input type="text" name="seatNumber" value="${seatNumber}"/> <br/>
<p/>
    <input type="submit" value="Add new hall"/>
</form>

<p><a href="${pageContext.servletContext.contextPath}/admin/hallsAdm">Список залов</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>
</body>
</html>
