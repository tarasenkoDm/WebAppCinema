<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<h3>Edit movie</h3>
${description} ${descriptionExist}<p/>

<form name="movieEditByAdmin" method="post" action="${pageContext.servletContext.contextPath}/admin/movieEditAdm">
    <input type="hidden" name="id" value="${movieDTO.id}">
    Title:<br> <input id="title" type="text" name="title" value="${movieDTO.title}"/> <br/>
    Description:<br> <input id="description" type="text" name="description" value="${movieDTO.description}"/> <br/>
    Duration:<br> <input id="duration" type="text" name="duration" value="${movieDTO.duration}"/> <br/>
    RentStart:<br><input id="rentStart" type="date" name="rentStart" value="${movieDTO.rentStart}"/></br>
    RentEnd:<br><input id="rentEnd" type="date" name="rentEnd" value="${movieDTO.rentEnd}"/></br>
    Genre:<br> <input id="genre" type="text" name="genre" value="${movieDTO.genre}"/> <br/>
    Rating:<br> <input id="rating" type="text" name="rating" value="${movieDTO.rating}"/> <br/>


    <p/>
    <input type="submit" value="Edit movie"/>
</form>

<p><a href="${pageContext.servletContext.contextPath}/admin/moviesAdm">Список фильмов</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>
</body>
</html>
