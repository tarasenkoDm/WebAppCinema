<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <title>Add new Movie by admin</title>
</head>
<body>

<h3>Add new movie</h3>
${description} ${descriptionExist}<p/>

<form name="movieAddByAdmin" method="post" action="${pageContext.servletContext.contextPath}/admin/movieAddAdm">
    Title:<br> <input id="title" type="text" name="title" value="${title}"/> <br/>
    Description:<br> <input id="description" type="text" name="description"/> <br/>
    Duration:<br> <input id="duration" type="text" name="duration" value="${duration}"/> <br/>
    RentStart:<br><input id="rentStart" type="date" name="rentStart" value="${rentStart}"/></br>
    RentEnd:<br><input id="rentEnd" type="date" name="rentEnd" value="${rentEnd}"/></br>
    Genre:<br> <input id="genre" type="text" name="genre" value="${genre}"/> <br/>
    Rating:<br> <input id="rating" type="text" name="rating" value="${rating}"/> <br/>


<p/>
    <input type="submit" value="Add new movie"/>
</form>

<p><a href="${pageContext.servletContext.contextPath}/admin/moviesAdm">Список фильмов</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin/adm">Admin page</a></p>
</body>
</html>
