<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Edit Session</title>
</head>
<body>
<h3>Edit session for ${movieDTO.title}</h3>

${login} ${loginExist}<p/>

<form name="sessionEditByAdmin" method="post" action="${pageContext.servletContext.contextPath}/admin/sessionEditAdm">
    <input type="hidden" name="sessionId" value="${sessionDTO.id}">
    <input type="hidden" name="movieId" value="${movieDTO.id}">
    Date:<br> <input type="date" name="sessionTime" value="${sessionDTO.sessionTime}"/><br>
    Start time:<br> <input type="time" name="timeStart" value="${sessionDTO.timeStart}"/><br>
    Price:<br> <input type="text" name="price" value="${sessionDTO.price}"/> <br/>
    Hall:<br> <select name="hallName" >
        <option value="${sessionDTO.hall.hallName}">${sessionDTO.hall.hallName}</option>
        <c:forEach items="${hallDTOList}" var="halls">
            <option value="${halls.hallName}">${halls.hallName}</option>
        </c:forEach>
    </select><br>
    <input type="hidden" name="title" value="${movieDTO.title}">

    <p/>
    <input type="submit" value="Edit session"/>
</form>
<p><a href="${pageContext.servletContext.contextPath}/admin/sessionAdm?id=${movieDTO.id}">Сеансы фильма ${movieDTO.title}</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin/moviesAdm">Список фильмов</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>

</body>
</html>
