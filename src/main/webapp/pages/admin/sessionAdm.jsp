<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Sessions Admin</title>
</head>
<body>
<h1> Сеансы фильма ${movieDTO.title}</h1>

<p><a href="${pageContext.servletContext.contextPath}/admin/sessionAddAdm?id=${movieDTO.id}">Добавить новое время проката</a></p>

<c:choose>
    <c:when test="${sessionDTOList == null}">
        Фильм ${movieDTO.title} еще не вышел в прокат.
    </c:when>
    <c:otherwise>

        <table border="1">
            <thread>
                <tr>
                    <th>Date</th>
                    <th>Start time</th>
                    <th>Price</th>
                    <th>Hall</th>
                    <th>Action</th>
                    <th>Action</th>
                </tr>
            </thread>
            <tbody>
            <c:forEach items="${sessionDTOList}" var="sessions">
                <tr>
                    <td><c:out value="${sessions.sessionTime}"/></td>
                    <td><c:out value="${sessions.timeStart}"/></td>
                    <%--<td><a href="${pageContext.servletContext.contextPath}/admin/ticket?id=${sessions.id}">${sessions.timeStart}</a></td>--%>
                    <td><c:out value="${sessions.price}"/></td>
                    <td><c:out value="${sessions.hall.hallName}"/></td>
                    <td><p><a href="${pageContext.servletContext.contextPath}/admin/sessionEditAdm?id=${sessions.id}">Редактировать</a></p></td>
                    <td><p><a href="${pageContext.servletContext.contextPath}/admin/sessionDeleteAdm?id=${sessions.id}">Удалить</a></p></td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </c:otherwise>

</c:choose>

<p><a href="${pageContext.servletContext.contextPath}/admin/moviesAdm">Список фильмов</a></p>
<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>

</body>
</html>
