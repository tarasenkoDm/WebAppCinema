<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>All Movies</title>
</head>
<body>
<h1>All Movies</h1>

<p><a href="${pageContext.servletContext.contextPath}/admin/movieAddAdm">Добавить новый фильм</a></p>

<table border="1">
    <thread>
        <tr>
            <th>Movie title</th>
            <th>Description</th>
            <th>Duration</th>
            <th>Start</th>
            <th>End</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th>
            <th>Action</th>
        </tr>
    </thread>
    <tbody>
    <c:forEach items="${movieDTOList}" var="movies">
        <tr>
            <td><a href="${pageContext.servletContext.contextPath}/admin/sessionAdm?id=${movies.id}">${movies.title}</a></td>
            <td><c:out value="${movies.description}"/></td>
            <td><c:out value="${movies.duration}"/></td>
            <td><c:out value="${movies.rentStart}"/></td>
            <td><c:out value="${movies.rentEnd}"/></td>
            <td><c:out value="${movies.genre}"/></td>
            <td><c:out value="${movies.rating}"/></td>
            <td><p><a href="${pageContext.servletContext.contextPath}/admin/movieEditAdm?id=${movies.id}">Редактировать</a></p></td>
            <td><p><a href="${pageContext.servletContext.contextPath}/admin/movieDeleteAdm?id=${movies.id}">Удалить</a></p></td>
        </tr>
    </c:forEach>

    </tbody>


</table>

<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>

</body>
</html>
