<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>All Halls</title>
</head>
<body>
<h1>List of Halls</h1>

<p><a href="${pageContext.servletContext.contextPath}/admin/hallAddAdm">Добавить новый зал</a></p>

<table border="1">
    <thread>
        <tr>
            <th>Hall name</th>
            <th>Rows</th>
            <th>Seats</th>
            <th>Action</th>
            <th>Action</th>
        </tr>
    </thread>
    <tbody>
    <c:forEach items="${hallDTOList}" var="halls">
        <tr>
            <td><c:out value="${halls.hallName}"/></td>
            <c:forEach items="${rowDTOList}" var="row">
                <c:if test="${halls.id == row.hall.id}">
                    <td><c:out value="${row.rowNumber}"/></td>
                    <td><c:out value="${row.seatNumber}"/></td>
                </c:if>
            </c:forEach>
            <td><p><a href="${pageContext.servletContext.contextPath}/admin/hallEditAdm?id=${halls.id}">Редактировать</a></p></td>
            <td><p><a href="${pageContext.servletContext.contextPath}/admin/hallDeleteAdm?id=${halls.id}">Удалить</a></p></td>
        </tr>
    </c:forEach>

    </tbody>


</table>

<p><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></p>
</body>
</html>
